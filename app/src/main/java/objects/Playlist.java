package objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matan on 04/07/2016.
 */
public class Playlist implements ParentListItem, Parcelable {
    private String listTitle;

    private ArrayList<InnerListItem> innerListItems;

    public Playlist(String listTitle) {
        this.listTitle = listTitle;
    }

    public Playlist(Parcel in) {
        readFromParcel(in);
    }

    public String getListTitle() {
        return listTitle;
    }

    public void setListTitle(String listTitle) {
        this.listTitle = listTitle;
    }

    public ArrayList<InnerListItem> getListItems() {
        if(innerListItems == null)
            return new ArrayList<>();
        else
            return innerListItems;
    }

    public void setListItems(ArrayList<InnerListItem> innerListItems) {
        this.innerListItems = innerListItems;
    }

    public void addListItem(InnerListItem innerListItem) {
        if(innerListItems == null)
            innerListItems = new  ArrayList<>();
        innerListItems.add(innerListItem);
    }

    @Override
    public List<?> getChildItemList() {
        return getListItems();
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(getListItems());
        dest.writeString(getListTitle());
    }

    private void readFromParcel(Parcel in) {
        setListItems(in.readArrayList(null));
        setListTitle(in.readString());
    }

    public static Parcelable.Creator<Playlist> CREATOR = new Parcelable.Creator<Playlist>() {
        @Override
        public Playlist createFromParcel(Parcel source) {
            return new Playlist(source);
        }

        @Override
        public Playlist[] newArray(int size) {
            return new Playlist[size];
        }
    };
}