package objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Matan on 04/07/2016.
 */
public class InnerListItem implements Parcelable {
    private String link;

    private String thumb;

    private String Title;

    public InnerListItem(String link, String thumb, String title) {
        this.link = link;
        this.thumb = thumb;
        Title = title;
    }

    public InnerListItem(Parcel in) {
        readFromParcel(in);
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getTitle());
        dest.writeString(getLink());
        dest.writeString(getThumb());
    }

    private void readFromParcel(Parcel in) {
        setTitle(in.readString());
        setLink(in.readString());
        setThumb(in.readString());
    }

    public static Parcelable.Creator<InnerListItem> CREATOR = new Parcelable.Creator<InnerListItem>() {
        @Override
        public InnerListItem createFromParcel(Parcel source) {
            return new InnerListItem(source);
        }

        @Override
        public InnerListItem[] newArray(int size) {
            return new InnerListItem[size];
        }
    };
}
