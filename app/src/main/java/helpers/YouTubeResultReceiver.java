package helpers;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

/**
 * Created by Matan on 01/07/2016.
 */
public class YouTubeResultReceiver extends ResultReceiver {
    private Received mReceiver;

    public YouTubeResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Received receiver) {
        mReceiver = receiver;
    }

    public interface Received {
        void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
