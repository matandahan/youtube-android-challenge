package helpers;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import objects.InnerListItem;
import objects.Playlist;

/**
 * Created by Matan on 01/07/2016.
 */
public class YouTubeIntentService extends IntentService {
    private static final String TAG = "YouTubeIntentService";

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public YouTubeIntentService() {
        super(YouTubeIntentService.class.getName());
        Log.d(TAG, "Service Created!");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Service Started!");

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String url = "http://www.razor-tech.co.il/hiring/youtube-api.json";
        Bundle bundle = new Bundle();

        if (isNetworkOnline()) {
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);

            try {
                ArrayList<Playlist> playLists = parseYouTubeJson(url);
                Log.d(TAG, "Service Success!");
                bundle.putParcelableArrayList("result", playLists);
                receiver.send(STATUS_FINISHED, bundle);
            } catch (Exception e) {
                e.printStackTrace();
                bundle.putString(Intent.EXTRA_TEXT, "There was a problem connecting to the requested address...");
                receiver.send(STATUS_ERROR, bundle);
            }
        } else {
            Log.d(TAG, "Service Error!");
            bundle.putString(Intent.EXTRA_TEXT, "There was a problem connecting to the requested address...");
            receiver.send(STATUS_ERROR, bundle);
        }
        Log.d(TAG, "Service Stopping!");
        this.stopSelf();
    }

    private ArrayList<Playlist> parseYouTubeJson(String requestUrl) throws IOException {
        URL url = new URL(requestUrl);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        int statusCode = urlConnection.getResponseCode();

        if (statusCode == 200) {
            ArrayList<Playlist> playLists = new ArrayList<>();
            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            try{
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                StringBuilder responseStrBuilder = new StringBuilder();

                String inputStr;
                while ((inputStr = streamReader.readLine()) != null) {
                    responseStrBuilder.append(inputStr);
                }
                String playListsJsonString = responseStrBuilder.toString();
                Log.d(TAG, "PlayLists Json - " + playListsJsonString);

                JSONObject playListsJson = new JSONObject(playListsJsonString);
                JSONArray playListsJsonArray = playListsJson.getJSONArray("Playlists");
                Log.d(TAG, "PlayLists - " + playListsJsonArray.length());

                for(int i=0 ; i<playListsJsonArray.length() ; i++){
                    JSONObject singlePlayListJson = (JSONObject) playListsJsonArray.get(i);
                    Playlist playlist = new Playlist(singlePlayListJson.optString("ListTitle"));
                    Log.d(TAG, "PlayList Added - " + playlist.getListTitle());

                    JSONArray playListInnerJsonArray = singlePlayListJson.getJSONArray("ListItems");
                    Log.d(TAG, "PlayList Items - " + playListInnerJsonArray.length());

                    for(int j=0 ; j<playListInnerJsonArray.length() ; j++){
                        JSONObject innerPlayListItemJson = (JSONObject) playListInnerJsonArray.get(j);
                        String title = innerPlayListItemJson.optString("Title");
                        String link = innerPlayListItemJson.optString("link");
                        String thumb = innerPlayListItemJson.optString("thumb");
                        InnerListItem innerListItem = new InnerListItem(link, thumb, title);
                        playlist.addListItem(innerListItem);
                    }
                    playLists.add(playlist);
                }
                Log.d(TAG, "PlayLists Count - " + playLists.size());
                return playLists;
            }catch (Exception e){
                e.printStackTrace();
                return playLists;
            }
        } else {
            throw new IOException("Failed to fetch data!!");
        }
    }

    public boolean isNetworkOnline() {
        Log.d(TAG, "Check Network");
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        Log.d(TAG, "Have WIFI - " + haveConnectedWifi);
        Log.d(TAG, "Have 3G - " + haveConnectedMobile);
        return haveConnectedWifi || haveConnectedMobile;
    }
}
