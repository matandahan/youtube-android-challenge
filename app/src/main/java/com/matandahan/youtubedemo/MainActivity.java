package com.matandahan.youtubedemo;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;

import adapters.YouTubePlayListAdapter;
import helpers.YouTubeIntentService;
import helpers.YouTubeResultReceiver;
import objects.Playlist;
import views.InnerListItemViewHolder;

public class MainActivity extends YouTubeBaseActivity implements
        YouTubeResultReceiver.Received, InnerListItemViewHolder.ThumbClick, YouTubePlayer.OnInitializedListener{
    private static final String TAG = "MainActivity";
    public static final String API_KEY = "AIzaSyDHC1d6ubIOQ3XaU1y1TRQq--SwLRppuRk";
    private static final int RECOVERY_DIALOG_REQUEST = 1234;

    private ProgressBar loadProgress;
    private RecyclerView playListsRecycler;
    private YouTubePlayerView youTubeView;

    private ArrayList<Playlist> mPlayLists;
    private Playlist selectedPlayList;
    private String LAST_PLAYD_LINK = "";
    private int LAST_PLAYED_POSITION = 0;

    private YouTubePlayer mYouTubePlayer;
    private YouTubeResultReceiver mYouTubeResultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        fetchPlayLists();
    }

    private void initViews() {
        if (loadProgress == null)
            loadProgress = (ProgressBar) findViewById(R.id.load_progress);
        if (playListsRecycler == null)
            playListsRecycler = (RecyclerView) findViewById(R.id.playlists_recycler);
        if (youTubeView == null)
            youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);

        youTubeView.initialize(API_KEY, MainActivity.this);
    }

    private void fetchPlayLists() {
        if (mPlayLists == null) {
            loadProgress.setVisibility(View.VISIBLE);
            if (mYouTubeResultReceiver == null)
                mYouTubeResultReceiver = new YouTubeResultReceiver(new Handler());

            mYouTubeResultReceiver.setReceiver(MainActivity.this);

            Intent intent = new Intent(Intent.ACTION_SYNC, null, MainActivity.this, YouTubeIntentService.class);
            intent.putExtra("receiver", mYouTubeResultReceiver);
            startService(intent);
            Log.d(TAG, "fetchPlayLists");
        } else {
            initPlayListsUI();
        }
    }

    private void initPlayListsUI(){
        Log.d(TAG, "initPlayListsUI");
        if (mPlayLists != null) {
            loadProgress.setVisibility(View.GONE);
            YouTubePlayListAdapter youTubePlayListAdapter = new YouTubePlayListAdapter(this, mPlayLists, this);
            youTubePlayListAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
                @Override
                public void onListItemExpanded(int position) {
                    selectedPlayList = mPlayLists.get(position);
                }

                @Override
                public void onListItemCollapsed(int position) {
                    selectedPlayList = null;
                }
            });
            playListsRecycler.setAdapter(youTubePlayListAdapter);
            playListsRecycler.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    @Override
    public void onThumbClick(String link) {
        //Which youtube thumb was clicked by the user
        if(selectedPlayList != null) {
            String substractString = "https://www.youtube.com/watch?v=";//Remove link prefix and leave only the id of video
            Log.d(TAG, "Link To Play - " + link);
            String linkToPlay = link.substring(substractString.length());
            youTubeView.setVisibility(View.VISIBLE);
            mYouTubePlayer.loadVideo(linkToPlay);

            if(LAST_PLAYD_LINK.equals(link))
                mYouTubePlayer.seekToMillis(LAST_PLAYED_POSITION);
            else
                LAST_PLAYD_LINK = link;

            mYouTubePlayer.play();
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case YouTubeIntentService.STATUS_RUNNING:
                setProgressBarIndeterminateVisibility(true);
                break;
            case YouTubeIntentService.STATUS_FINISHED:
                mPlayLists = resultData.getParcelableArrayList("result");
                initPlayListsUI();
                setProgressBarIndeterminateVisibility(false);
                break;
            case YouTubeIntentService.STATUS_ERROR:
                setProgressBarIndeterminateVisibility(false);
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //Prevent reloading and making trouble if not absolutely necessary...
    }

    @Override
    public void onBackPressed() {
        if(youTubeView.getVisibility() == View.VISIBLE) {
            LAST_PLAYED_POSITION = mYouTubePlayer.getCurrentTimeMillis();
            mYouTubePlayer.pause();
            youTubeView.setVisibility(View.GONE);
        }else
            super.onBackPressed();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored && mYouTubePlayer == null) {
            mYouTubePlayer = youTubePlayer;
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    "YouTube Player Error", youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            youTubeView.initialize(API_KEY, this);
        }
    }
}
