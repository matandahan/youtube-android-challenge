package views;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bumptech.glide.Glide;
import com.matandahan.youtubedemo.R;

import objects.InnerListItem;

/**
 * Created by Matan on 04/07/2016.
 */
public class InnerListItemViewHolder extends ChildViewHolder {

    private TextView mInnerItemTitle;
    private ImageView mInnerThumbView;

    private InnerListItemViewHolder.ThumbClick thumbClickCallback;

    public InnerListItemViewHolder(View itemView) {
        super(itemView);
        mInnerItemTitle = (TextView) itemView.findViewById(R.id.inner_item_title);
        mInnerThumbView = (ImageView) itemView.findViewById(R.id.inner_item_thumb);
    }

    public void bind(Context context, final ThumbClick thumbClickCallback, final InnerListItem innerListItem) {
        this.thumbClickCallback = thumbClickCallback;
        mInnerItemTitle.setText(innerListItem.getTitle());
        Glide.with(context).load(innerListItem.getThumb()).into(mInnerThumbView);

        mInnerThumbView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thumbClickCallback.onThumbClick(innerListItem.getLink());
            }
        });
    }

    public interface ThumbClick {
        void onThumbClick(String link);
    }
}
