package views;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.matandahan.youtubedemo.R;

import objects.Playlist;

/**
 * Created by Matan on 04/07/2016.
 */
public class PlayListViewHolder extends ParentViewHolder {

    private TextView mPlayListText;

    public PlayListViewHolder(View itemView) {
        super(itemView);
        mPlayListText = (TextView) itemView.findViewById(R.id.play_list_item);
    }

    public void bind(Playlist playlist) {
        mPlayListText.setText(playlist.getListTitle());
    }
}
