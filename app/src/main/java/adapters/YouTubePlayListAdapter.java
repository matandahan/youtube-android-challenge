package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.matandahan.youtubedemo.R;

import java.util.ArrayList;

import objects.InnerListItem;
import objects.Playlist;
import views.InnerListItemViewHolder;
import views.PlayListViewHolder;

/**
 * Created by Matan on 04/07/2016.
 */
public class YouTubePlayListAdapter extends ExpandableRecyclerAdapter<PlayListViewHolder, InnerListItemViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private InnerListItemViewHolder.ThumbClick thumbClickCallback;

    public YouTubePlayListAdapter(Context context, ArrayList<Playlist> parentItemList,
                                  InnerListItemViewHolder.ThumbClick thumbClickCallback) {
        super(parentItemList);
        this.mContext = context;
        this.thumbClickCallback = thumbClickCallback;
        mInflator = LayoutInflater.from(context);
    }

    // onCreate ...
    @Override
    public PlayListViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View playListView = mInflator.inflate(R.layout.playlist_parent_layout, parentViewGroup, false);
        return new PlayListViewHolder(playListView);
    }

    @Override
    public InnerListItemViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View innerItemView = mInflator.inflate(R.layout.youtube_item_layout, childViewGroup, false);
        return new InnerListItemViewHolder(innerItemView);
    }

    @Override
    public void onBindParentViewHolder(PlayListViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        parentViewHolder.bind((Playlist) parentListItem);
    }

    @Override
    public void onBindChildViewHolder(InnerListItemViewHolder childViewHolder, int position, Object childListItem) {
        childViewHolder.bind(mContext, thumbClickCallback, (InnerListItem) childListItem);
    }

}
